#+TITLE:      git configuration
#+AUTHOR:     Toon Claes
#+KEYWORDS:   toon claes git config
#+STARTUP:    showall
-----

* git config
:PROPERTIES:
:header-args: :tangle ~/.config/git/config
:header-args+: :comments both :mkdirp yes
:END:

This file generates my git global config.

** User config

Only define my name, and not my email. And instruct git to avoid
trying to guess a default for the email. This will enforce me to set
the email for each git repo.

#+BEGIN_SRC conf
[user]
        useConfigOnly = true
        name = Toon Claes
#+END_SRC

** Core config

#+BEGIN_SRC conf
[core]
#+END_SRC

*** Global ignore file

Use a global ignore file.

I'm using the default =~/.config/git/ignore= so no need to set
this explicitly.

#+BEGIN_SRC conf
        excludesfile = ~/.config/git/ignore
#+END_SRC

*** Commit comments

I tend to use markdown in my commit messages, and markdown uses the
=#= symbol to define headings. So instead of the default symbol =#=
used to define comments in commit messages, use =;=.

#+BEGIN_SRC conf
        commentchar = ";"
#+END_SRC

*** CR/LF

Don't do output conversion on end-of-line.

#+BEGIN_SRC conf
        autocrlf = input
#+END_SRC


** Help

#+begin_src
[help]
#+end_src

*** Auto-correct

Auto-correct mistyped commands after 0.1 seconds.

#+begin_src conf
        autoCorrect = 1
#+end_src

** Init

#+begin_src conf
[init]
#+end_src

*** Default branch

When creating a new repo, set the default branch to =main=.

#+begin_src conf
        defaultBranch = main
#+end_src

** Commit

#+begin_src conf
[commit]
#+end_src

*** Template

Specify a git commit template that will help me to write good commit messages.

Here we start with an empty block to force org-tangle to start the file with an
empty line. The ~padline~ header argument, defaulting to ~yes~, causes empty
lines between the above and the below codeblock.

#+begin_src conf-windows :tangle ~/.config/git/commit_msg.txt :comments no
#+end_src

#+begin_src conf-windows :tangle ~/.config/git/commit_msg.txt :comments no
; Why is this change needed?
;Prior to this change,

; How does it address the issue?
;Let's ...

; Provide links to any relevant tickets, articles or other resources


;Label: bug::performance
;Label: bug::availability
;Label: bug::vulnerability
;Label: bug::mobile
;Label: bug::functional
;Label: bug::ux
;Label: bug::transient

;Label: feature::addition
;Label: feature::enhancement
;Label: feature::consolidation

;Label: maintenance::refactor
;Label: maintenance::removal
;Label: maintenance::dependency
;Label: maintenance::scalability
;Label: maintenance::usability
;Label: maintenance::test-gap
;Label: maintenance::pipelines
;Label: maintenance::workflow
;Label: maintenance::performance
;Label: maintenance::release

;Changelog: added
;Changelog: fixed
;Changelog: changed
;Changelog: deprecated
;Changelog: removed
;Changelog: security
;Changelog: performance
;Changelog: other

;EE: true

#+end_src

And configure git to use this template.

#+begin_src conf
        template = ~/.config/git/commit_msg.txt
#+end_src

** Status

#+begin_src conf
[status]
#+end_src

*** Untracked files

Show also individual files in untracked directories.

#+begin_src conf
        showUntrackedFiles = all
#+end_src

*** Submodule summary

Enable submodule summary showing the summary of commits for modified
submodules.

#+begin_src conf
        submoduleSummary = true
#+end_src

** Diff

#+begin_src conf
[diff]
#+end_src

*** Moved code

Show moved lines in a different color.

#+begin_src conf
        colorMoved = zebra
#+end_src

** Merge

#+begin_src conf
[merge]
#+end_src

** Mergiraf

Use [[https://mergiraf.org/][mergiraf]] for better automatic merge conflict resolving.

#+begin_src conf
	name = mergiraf
	driver = mergiraf merge --git %O %A %B -s %S -x %X -y %Y -p %P
#+end_src

** Fetching

#+begin_src conf
[fetch]
#+end_src

*** Prune

Prune the local tracking branches and tags when fetching from remote.

#+begin_src conf
        prune = true
        pruneTags = true
#+end_src

** Pull

#+begin_src conf
[pull]
#+end_src

*** Fast-forward only

When pulling only update the current branch by fast-forwarding.

#+begin_src conf
        ff = only
#+end_src

** Transfer

#+begin_src conf
[transfer]
#+end_src

*** Bundle-URI

Enable the use of Bundle-URI when the server advertises it.

#+begin_src conf
        bundleURI = true
#+end_src

** Aliases

Define a set of aliases.

#+BEGIN_SRC conf
[alias]
#+END_SRC

*** Scrub

This alias scrubs away all local branches that are merged.

#+BEGIN_SRC conf
        scrub = !git branch --merged | grep --extended-regexp --invert-match '(^\\*|master|main)' | xargs --no-run-if-empty git branch --delete
#+END_SRC

*** Work-in-progress

I don't like git stashes. [[https://twitter.com/to1ne/status/993495155234738177][A git stash is where code goes to die]]. So
instead of stashes, [[https://haacked.com/archive/2014/07/28/github-flow-aliases/][create a WIP commit]] on the current branch. This is
much better to keep context of the work-in-progress code, allows me to
push the code to a remote and have backup, and maybe also give me some
insights on the test results of CI.

I'm also providing an alias that undoes the last commit if it is a WIP
commit.

Improved =unwip= alias [[https://twitter.com/ZJvandeWeg/status/1050310776353886209][by ZJ]]. Improved =wip= from
[[https://itnext.io/multitask-like-a-pro-with-the-wip-commit-2f4d40ca0192][Aziz Nal]], who took the idea from [[https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git][ohmyzsh]], which might have gotten it from
[[https://haacked.com/archive/2014/07/28/github-flow-aliases/][@haacked]].

#+BEGIN_SRC conf
        wip = !git add --all && git commit --no-verify --no-gpg-sign --message 'WIP [skip ci]'
        unwip = !git log --max-count=1 --format=format:"%H" --invert-grep --grep '^WIP' | xargs git reset --soft
#+END_SRC

*** Rebase on remote master

I commonly rebase my working branch on =master= on the remote. That
remote can be =origin= or =upstream=. So this alias allows me to fetch
=master= from the =<remote>= and rebase current branch on
=<remote>/master=.

It expects one argument:
 + the name of the remote

#+BEGIN_SRC conf
        remaster = !sh -c 'git fetch $1 master && git rebase $1/master' -
#+END_SRC

*** Resync with remote

When the remote branch was rebased and you want to resynchronize your
local branch to the remote state, use this =reremote= alias.

It expects one argument:
 + the name of the remote

#+BEGIN_SRC conf
        reremote = !sh -c 'git rev-parse --abbrev-ref HEAD | xargs git fetch $1 && git reset --hard FETCH_HEAD' -
#+END_SRC

*** Fetch and checkout

Fetch the branch from the remote, create a tracking branch for it and
check it out now 🎶the funk soul brother🎶.

It expects two arguments:
 + the name of the remote
 + the name of the branch

#+begin_src conf
	cofetch = !sh -c 'git fetch $1 $2:remotes/$1/$2 && git switch -c $2 remotes/$1/$2' -
#+end_src

*** Politely force push current branch

When force pushing it's safer to use Git's =--force-with-lease= as this ensures
the latest changes were fetched before overwriting changes on the remote.

#+begin_src conf
	plush = !sh -c 'git push --force-with-lease $1 HEAD' -
#+end_src

*** Loglog

Fancy shortlog.

#+begin_src conf
	loglog = log --graph --oneline --all
#+end_src

** Rerere

git-rerere stands for: Reuse recorded resolution of conflicted merges.
It is an awesome feature that helps you to resolve the same conflicts
over and over again.

#+BEGIN_SRC conf
[rerere]
        enabled = true
#+END_SRC

** Markdown diffing

Below is an attempt to improve diffing markdown files. But I never got
it working and I just left it here.

#+BEGIN_SRC conf :tangle no
[diff "markdown"]
        tool = lowdown

[difftool "lowdown"]
        cmd = "lowdown-diff -s -Tman $LOCAL $REMOTE | groff -Tascii -man | less"
#+END_SRC

** Emacs diff tool

Use emacs(client) as a difftool.

#+begin_src conf
[difftool "ediffclient"]
        cmd = emacsclient --eval \"(ediff-files \\\"$LOCAL\\\" \\\"$REMOTE\\\")\"
#+end_src

** LFS

These settings involving LFS are generated by git itself.

#+BEGIN_SRC conf
[filter "lfs"]
        smudge = git-lfs smudge -- %f
        process = git-lfs filter-process
        required = true
        clean = git-lfs clean -- %f
#+END_SRC

** Protocol

Enable git protocol version 2.

Read [[https://about.gitlab.com/2018/12/10/git-protocol-v2-enabled-for-ssh-on-gitlab-dot-com/][about it]].

#+BEGIN_SRC conf
[protocol]
        version = 2
#+END_SRC

** Sendmail

Configure git to send mails directly from git.

Instructions came from [[https://git-send-email.io/][git-send-email.io]].

I have =msmtp= configured to send mails through Emacs, so I can just
use that tool here too.

#+begin_src conf
[sendemail]
    smtpserver = /usr/bin/msmtp
#+end_src

** B4

I'm using [[https://b4.docs.kernel.org/en/latest/index.html][b4]] to send patches to the Git mailing list. This can use some
configuration.

#+begin_src conf
[b4]
#+end_src

*** Signing

Don't sign mails with patatt.

#+begin_src conf
	send-no-patatt-sign = yes
#+end_src

** Skip auto-CC

On the Git mailing list there isn't an auto-CC script, so skip that step.

#+begin_src conf
	prep-pre-flight-checks = disable-needs-auto-to-cc
#+end_src

* Global Gitignore
:PROPERTIES:
:header-args: :tangle ~/.config/git/ignore
:header-args+: :comments both :mkdirp yes
:END:

** Ruby vendoring

Ignore gems installed in the local =vendor= directory.

#+BEGIN_SRC conf
**/vendor/bundle
#+END_SRC

** GNU Global

For a while I used GNU Global for tagging code, so ignore the =TAGS=
files from being committed.

#+BEGIN_SRC conf
GPATH
GRTAGS
GTAGS
TAGS
#+END_SRC

** CCLS LSP

For C code I'm using CCLS with LSP in emacs. Ignore the files this
creates.

#+begin_src conf
.ccls-cache/
#+end_src

** Emacs

*** Dir locals

Most project don't like Emacs =.dir-locals.el=.

#+BEGIN_SRC conf
.dir-locals.el
#+END_SRC

*** Auto-save files

Emacs creates auto-save files named by appending ~#~ to the front and rear of
the visited file name.

#+BEGIN_SRC conf
\#*\#
#+END_SRC


** GitLab & GitHub

Do *not* ignore =.gitlab= and =.gitlab-ci.yml=, and be explicit about it. This
will also make =rg(1)= search these. And do the same for GitHub's files.

#+begin_src conf
!.gitlab
!.gitlab-ci.yml
!.github
#+end_src

** Compile flags

For LSP in Emacs you might need to specify extra compiler flags, but
not all projects have it, so ignore files setting them.

#+begin_src conf
compile_flags.txt
.ccls
#+end_src

There is also =compile_commands.json=, which needs to be generated. You can
either use Bear to generate it. This is done by prefixing the ~make~ command
with ~bear --~, or tools like Meson generate it automatically.

#+begin_src conf
compile_commands.json
#+end_src

** Byebug

Byebug creates a history file in the current working directory, and
that's not something you want to check in.

#+BEGIN_SRC conf
.byebug_history
#+END_SRC


* Git attributes
:PROPERTIES:
:header-args: :tangle ~/.config/git/attributes
:header-args+: :comments both :mkdirp yes
:END:

** Diff

Improve diff output for various file types.

From: https://tekin.co.uk/2020/10/better-git-diff-output-for-ruby-python-elixir-and-more

#+BEGIN_SRC conf
*.c     diff=cpp
*.h     diff=cpp
*.c++   diff=cpp
*.h++   diff=cpp
*.cpp   diff=cpp
*.hpp   diff=cpp
*.cc    diff=cpp
*.hh    diff=cpp
*.cs    diff=csharp
*.css   diff=css
*.html  diff=html
*.xhtml diff=html
*.ex    diff=elixir
*.exs   diff=elixir
*.go    diff=golang
*.php   diff=php
*.pl    diff=perl
*.py    diff=python
*.md    diff=markdown
*.rb    diff=ruby
*.rake  diff=ruby
*.rs    diff=rust
*.lisp  diff=lisp
*.el    diff=lisp
#+END_SRC

** Merge

Use ~mergiraf~ for automatically conflict resolution.

#+NAME: mergiraf-attr
#+begin_src sh :results output :tangle no
mergiraf languages --gitattributes
#+end_src

#+begin_src conf :noweb yes
<<mergiraf-attr()>>
#+end_src

* Hooks

User-wide hooks.

** Pre-push
:PROPERTIES:
:header-args: :tangle ~/.config/git/hooks/pre-push
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

When a =Gemfile= is found, and =rubocop= is enabled, run Rubocop on
the files modified compared to the last merge commit. And since in our
codebase everything is applied to master with a merge commit, this can
be considered the upstream commit.

We also could have used =@{upstream}=, but that requires each branch
to set it's =upstream= branch, and that is not always the case.

#+BEGIN_SRC sh :tangle no
if [[ -f Gemfile.lock && -x $(bundle exec which rubocop) ]]; then
  echo "> Running Rubocop.."
  git diff --name-only --diff-filter=d $(git log --merges -1 --pretty=format:%H) | xargs bundle exec rubocop
else
  exit 0
fi
#+END_SRC

* Standalone commands

A section of small, standalone git command scripts

** Abort
:PROPERTIES:
:header-args: :tangle ~/.local/bin/git-abort
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

A short command to abort any action that is ongoing. That could be:
 + cherry-pick
 + rebase
 + merge

#+BEGIN_SRC sh
git_status=$(git status)

if [[ $git_status =~ 'currently cherry-picking' ]]; then
  git cherry-pick --abort
  echo "Ongoing cherry-pick aborted."
  exit 0
fi

if [[ $git_status =~ $(echo 'currently (editing a commit while )?rebasing') ]]; then
  git rebase --abort
  echo "Ongoing rebase aborted."
  exit 0
fi

if [[ $git_status =~ 'you are still merging' ]]; then
  git merge --abort
  echo "Ongoing merge aborted."
  exit 0
fi

echo >&2 "Nothing found to abort."
exit 1
#+END_SRC

** Continue
:PROPERTIES:
:header-args: :tangle ~/.local/bin/git-continue
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

A short command to continue any action that is ongoing. That could be:
 + cherry-pick
 + rebase
 + merge

#+BEGIN_SRC sh
git_status=$(git status)

if [[ $git_status =~ 'currently cherry-picking' ]]; then
  echo "Continuing ongoing cherry-pick."
  git cherry-pick --continue
  exit 0
fi

if [[ $git_status =~ $(echo 'currently (editing a commit while )?rebasing') ]]; then
  git rebase --continue
  echo "Continuing ongoing rebase."
  exit 0
fi

if [[ $git_status =~ 'you are still merging' ]]; then
  echo "Continuing ongoing merge."
  git commit
  exit 0
fi

echo >&2 "Nothing found to continue."
exit 1
#+END_SRC

** Modified
:PROPERTIES:
:header-args: :tangle ~/.local/bin/git-modified
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

Get all files that are modified.

There was some discussion on this on [[https://gitlab.slack.com/archives/C02PF508L/p1537285816000100?thread_ts=1537250844.000100&cid=C02PF508L][team chat]].

#+begin_src sh
git diff --name-only --diff-filter=d
#+end_src

** Unlock
:PROPERTIES:
:header-args: :tangle ~/.local/bin/git-unlock
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

Sometimes your git can get locked, ~.git/index.lock~ in particular.

#+begin_src sh
rm .git/index.lock
#+end_src

** MR push
:PROPERTIES:
:header-args: :tangle ~/.local/bin/git-mr-push
:header-args+: :comments both :mkdirp yes :shebang "#!/bin/bash -e"
:END:

Push the current branch and create an MR. This uses the trailers from the commit
template to add tags to the MR.

#+begin_src sh
# Look up branch description first
branch="$(git rev-parse --abbrev-ref $commit)"
msg="$(git config branch.$branch.description || true)"

# Next try to find a commit that can be used as cover letter
commit="${COMMIT}"
if [ -z "${commit}" ]
then
  commit="$(git log --grep '--- b4-submit-tracking ---' --format=%H)"
fi
if [ -z "${commit}" ]
then
  commit="HEAD".
fi
if [ -z "${msg}" ]
then
  msg="$(git log -1 --format=%B ${commit})"
fi

title="$(echo "${msg}" | head -1)"
desc="$(echo "${msg}" | tail -n+3 | sed -z 's/\n/\\n/g')"
milestone="${MILESTONE:-$(git log -1 --format='%(trailers:key=Milestone,valueonly=true)' ${commit})}"
labels="$(git log -1 --format='%(trailers:key=Label,valueonly=true)' ${commit})"

opts=""

if [ -n "${milestone}" ]
then
  opts+=" -o merge_request.milestone=${milestone}"
fi

for l in ${labels}
do
  opts+=" -o merge_request.label=${l}"
done

opts+=" -o merge_request.label=group::git"

set -x

git push --set-upstream \
    -o merge_request.create \
    -o merge_request.title="${title}" \
    -o merge_request.description="${desc}" \
    $opts \
    ${@}
#+end_src
