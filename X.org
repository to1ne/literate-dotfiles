#+TITLE:      X11 configuration
#+AUTHOR:     Toon Claes
#+STARTUP:    showall
-----

* Xresources
:PROPERTIES:
:header-args: :tangle ~/.Xresources
:header-args+: :comments both :mkdirp yes
:END:

** HiDPI

Increase the DPI.

From:
 + https://dougie.io/linux/hidpi-retina-i3wm/
 + https://wiki.archlinux.org/index.php/HiDPI#X_Resources

#+BEGIN_SRC conf-xdefaults
*.dpi: 192
Xft.dpi: 192
Xft.antialias: true
Xft.hinting: true
Xft.rgba: rgb
Xft.autohint: false
! Xft.hintstyle: hintslight
Xft.hintstyle: hintfull
Xft.lcdfilter: lcddefault
#+END_SRC

Other possible sources:
 + https://www.valhalla.fr/2018/07/14/hidpi-on-gnome-desktop/
 + https://mike42.me/blog/2018-02-how-to-use-hidpi-displays-on-debian-9
 + https://wiki.manjaro.org/index.php?title=Improve_Font_Rendering
 + https://code.luasoftware.com/tutorials/linux/enable-hidpi-scaling-on-lubuntu/
