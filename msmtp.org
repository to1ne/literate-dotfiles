#+TITLE:      Toon's msmtp configuration
#+AUTHOR:     Toon Claes
#+KEYWORDS:   toon claes msmtp smtp config
#+STARTUP:    showall
#+PROPERTY:   header-args+ :comments both
#+PROPERTY:   header-args+ :mkdirp yes
#+PROPERTY:   header-args+ :tangle "~/.config/msmtp/config"
-----

* MSMTP configuration

This file configures =msmtp=, which I use to send mails.

** Defaults

#+begin_src conf
  # Set default values for all following accounts.
  defaults
  tls on
  logfile ~/.cache/msmtp/msmtp.log
  tls_trust_file /etc/ssl/certs/ca-certificates.crt
#+end_src

** Migadu

#+begin_src conf
account migadu
host smtp.migadu.com
port 587
protocol smtp
auth on
from toon@iotcl.com
user toon@iotcl.com
passwordeval "op read 'op://Private/Migadu.com/password'"
tls on
#+end_src

** Startmail

#+begin_src conf
account startmail
host smtp.startmail.com
port 587
protocol smtp
auth on
from toon@to1.studio
user toon@to1.studio
passwordeval "op read 'op://Private/StartMail-mail/password'"
tls on
#+end_src

*** Default account

Make this the default account

#+begin_src conf
  account default : migadu
#+end_src

* Resources

A list of resources I've found on the subject:

- https://aaronweb.net/2014/11/migrating-mail-between-imap-servers-using-mbsync/
- https://bloerg.net/2013/10/09/syncing-mails-with-mbsync-instead-of-offlineimap.html
- http://xdeb.org/node/1607
- http://www.emacswiki.org/emacs/GnusGmail
- http://roland.entierement.nu/blog/2010/09/08/gnus-dovecot-offlineimap-search-a-howto.html
- https://www.reddit.com/r/emacs/comments/3wkccd/moving_from_mu4e_to_gnus/
- https://github.com/redguardtoo/mastering-emacs-in-one-year-guide/blob/master/gnus-guide-en.org
- http://sachachua.com/blog/2008/05/geek-how-to-use-offlineimap-and-the-dovecot-mail-server-to-read-your-gmail-in-emacs-efficiently/
- https://henrikpingel.wordpress.com/2014/07/30/how-to-use-isync-and-the-dovecot-mail-server-to-read-your-gmail-in-emacs-efficiently/
- http://alvinalexander.com/mac-os-x/mac-osx-startup-crontab-launchd-jobs
